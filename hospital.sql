-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 10, 2020 at 05:43 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hospital`
--

-- --------------------------------------------------------

--
-- Table structure for table `add_doctor`
--

CREATE TABLE `add_doctor` (
  `Joining Date` date NOT NULL DEFAULT '0000-00-00',
  `ID` char(5) NOT NULL,
  `Name` varchar(20) NOT NULL,
  `Age` int(2) NOT NULL,
  `Gender` varchar(6) NOT NULL,
  `Username` varchar(20) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `Phone number` bigint(12) NOT NULL,
  `Maritial Status` varchar(10) NOT NULL,
  `Address` varchar(255) NOT NULL,
  `Email` varchar(40) NOT NULL,
  `Specialist` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `add_doctor`
--

INSERT INTO `add_doctor` (`Joining Date`, `ID`, `Name`, `Age`, `Gender`, `Username`, `Password`, `Phone number`, `Maritial Status`, `Address`, `Email`, `Specialist`) VALUES
('2020-06-10', '003', 'Paresh Shah', 25, 'Male', 'Paresh', 'paresh123', 842526586, 'UnMarried', '24/b goregav mumbai', 'paresh@gmail.com', 'Allergist'),
('2020-06-10', '004', 'Paresh Shah', 25, 'Male', 'Paresh', 'p123', 842526586, 'UnMarried', '24/b goregav mumbai', 'paresh@gmail.com', 'Allergist'),
('2019-10-30', '101', 'Partik Murthy', 29, 'Male', 'Partik', 'partik@123', 9015161828, 'UnMarried', 'Vasna,Ahmedabad', 'partik@gmail.com', 'Allergist'),
('2018-05-11', '1010', 'Hrithik Misra', 35, 'Male', 'Hrithik', 'hrm123', 8784858498, 'UnMarried', 'Motera,Ahmedabad', 'hrithik@gmail.com', 'Hematologist'),
('2020-03-21', '110', 'Nilam Magar', 32, 'Female', 'Nilam', 'nilam12', 9825262810, 'Married', 'Maninagar,Ahmedabad', 'nilam@gmail.com', 'Cardiologist'),
('2016-08-19', '1101', 'Smarth Jain', 25, 'Male', 'Smarth', 'sj123', 7894587990, 'Married', 'Chandkheda,Ahmedabad', 'smarth@gmail.com', 'Endocrinologist');

-- --------------------------------------------------------

--
-- Table structure for table `add_patient`
--

CREATE TABLE `add_patient` (
  `Date` date NOT NULL DEFAULT '0000-00-00',
  `ID` varchar(5) NOT NULL,
  `Name` varchar(20) NOT NULL,
  `Age` int(2) NOT NULL,
  `Gender` varchar(6) NOT NULL,
  `Address` varchar(255) NOT NULL,
  `Phone_number` bigint(10) NOT NULL,
  `Maritial_status` varchar(10) NOT NULL,
  `Disease` varchar(50) NOT NULL,
  `Doctor_Name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `add_patient`
--

INSERT INTO `add_patient` (`Date`, `ID`, `Name`, `Age`, `Gender`, `Address`, `Phone_number`, `Maritial_status`, `Disease`, `Doctor_Name`) VALUES
('2020-06-06', '1019', 'Usha Patel', 33, 'Female', 'Chekhla,Ahmedabad', 8555578042, 'UnMarried', 'Heart Attack', 'Nilam Magar'),
('2020-06-23', '111', 'Sunita Gupta', 31, 'Female', 'Mehsana,Gujarat', 8555626459, 'Married', 'Thyroid', 'Hrithik Misra'),
('2020-07-24', '119', 'Niketa Gadhvi', 32, 'Female', 'Rajkot,Gujarat', 9555577680, 'Married', 'Leukemia', 'Samarth Jain'),
('2020-01-15', '201', 'Sandip Sharma', 23, 'Male', 'Nehru Bridge, Ahmedabad', 9555592065, 'Married', 'Strokes', 'Vishal Ranganathan'),
('2020-02-16', '202', 'Jitendra Das', 24, 'Male', 'Lal Darwaja,Ahmedabad', 755599584, 'Married', 'Eczema', 'Pratik Murthy'),
('2020-05-07', '2020', 'Jayendra Narang', 34, 'Male', 'Vasna,Ahmedabad', 9855509156, 'Married', 'Asthma', 'Pratik Murthy'),
('2020-06-17', '203', 'Grishma Tamboli', 25, 'Female', 'Motera,Ahmedabad', 7555155489, 'UnMarried', 'Strokes', 'Vishal Ranganathan'),
('2020-05-16', '204', 'Gurdeep Sharma', 26, 'Male', 'SG Road,Ahmedabad', 9855518275, 'UnMarried', 'Hemophilia', 'Samarth Jain'),
('2020-01-22', '211', 'Lalitha Bachchan', 30, 'Female', 'Khanpur,Ahmedabad', 9155552705, 'Married', 'Parkinsons disease', 'Vishal Ranganathan'),
('2020-07-04', '232', 'Nilofer Patel', 24, 'Female', 'Shahibaug,Ahmedabad', 9255564543, 'UnMarried', 'Anemia', 'Samarth Jain'),
('2017-05-02', '313', 'Preeti Kumar', 26, 'Female', 'Ellise Bridge,Ahmedabad', 9455539064, 'Married', 'Calcium and Bone Disorders', 'Hrithik Misra'),
('2020-06-03', '333', 'Arundhati Anand', 25, 'Female', 'Baldana,Ahmedabad', 7555685215, 'Married', 'Irregular Heartbeat', 'Nilam Magar'),
('2020-02-21', '411', 'Sita Singh', 29, 'Female', 'Hathijan,Ahmedabad', 8555111016, 'Married', 'Auto Immune', 'Pratik Murthy'),
('2020-04-19', '604', 'Govinda Das', 27, 'Male', 'Vasna,Ahmedabad', 9655557649, 'UnMarried', 'Epilepsy', 'Vishal Ranganathan'),
('2020-03-20', '611', 'Satish Sharma', 28, 'Male', 'Vejalpur,Ahmedabad', 9155552705, 'UnMarried', 'Diabetes', 'Hrithik Misra'),
('2018-04-01', '616', 'Rajendra Misra', 27, 'Male', 'CG Road,Ahmedabad', 7555841366, 'Married', 'Heart Failure', 'Nilam Magar'),
('2019-03-27', '661', 'Shankar Jain', 28, 'Male', 'Bareja,Ahmedabad', 9710707891, 'Married', 'Sickle Cell', 'Samarth Jain'),
('2020-02-26', '766', 'Mayur Patel', 32, 'Male', 'Chandkheda,Ahmedabad', 9758597190, 'UnMarried', 'Infertility', 'Hrithik Misra'),
('2020-01-25', '769', 'Pramod Jain', 33, 'Male', 'Ashram Road,Ahmedabad', 7555731518, 'UnMarried', 'Insect sting allergies', 'Pratik Murthy'),
('2020-08-05', '923', 'Jaydev Korrapati', 30, 'Male', 'Maninagar,Ahmedabad', 8555767826, 'Married', 'High Blood Pressure', 'Nilam Magar');

-- --------------------------------------------------------

--
-- Table structure for table `add_receptionist`
--

CREATE TABLE `add_receptionist` (
  `Joining Date` date NOT NULL,
  `ID` varchar(5) NOT NULL,
  `Name` varchar(20) NOT NULL,
  `Age` int(2) NOT NULL,
  `Gender` varchar(6) NOT NULL,
  `Username` varchar(20) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `Phone number` int(50) NOT NULL,
  `Maritial Status` varchar(10) NOT NULL,
  `Address` varchar(255) NOT NULL,
  `Email` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `add_receptionist`
--

INSERT INTO `add_receptionist` (`Joining Date`, `ID`, `Name`, `Age`, `Gender`, `Username`, `Password`, `Phone number`, `Maritial Status`, `Address`, `Email`) VALUES
('2018-05-26', '2479', 'Payal', 42, 'Female', 'payal', 'payal1', 89567412, 'Married', 'A-402 goregav', 'payal@gmail.com'),
('2017-04-28', '3352', 'Roma', 21, 'Female', 'roma', 'roma2', 75857441, 'UnMarried', 'Danilimda', 'roma@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `admin_login`
--

CREATE TABLE `admin_login` (
  `Username` varchar(20) NOT NULL,
  `Password` varchar(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_login`
--

INSERT INTO `admin_login` (`Username`, `Password`) VALUES
('Admin', 'admin123');

-- --------------------------------------------------------

--
-- Table structure for table `doctor_login`
--

CREATE TABLE `doctor_login` (
  `Username` varchar(20) NOT NULL,
  `Password` varchar(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `doctor_login`
--

INSERT INTO `doctor_login` (`Username`, `Password`) VALUES
('A. P. shah', 'ap@123'),
('Devansh', 'dev8080'),
('Doctor', 'doc@123'),
('Hrithik', 'hrm123'),
('Nilam', 'nilam123'),
('Paresh', 'p123'),
('partik', 'partik12'),
('Smarth', 'sj123'),
('Vishal', 'vishal@1');

-- --------------------------------------------------------

--
-- Table structure for table `receptionist_login`
--

CREATE TABLE `receptionist_login` (
  `Username` varchar(20) NOT NULL,
  `Password` varchar(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `receptionist_login`
--

INSERT INTO `receptionist_login` (`Username`, `Password`) VALUES
('payal', 'payal1'),
('Receptionist', 'rec@123'),
('roma', 'roma2');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `add_doctor`
--
ALTER TABLE `add_doctor`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `add_patient`
--
ALTER TABLE `add_patient`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `add_receptionist`
--
ALTER TABLE `add_receptionist`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `admin_login`
--
ALTER TABLE `admin_login`
  ADD PRIMARY KEY (`Username`);

--
-- Indexes for table `doctor_login`
--
ALTER TABLE `doctor_login`
  ADD PRIMARY KEY (`Username`);

--
-- Indexes for table `receptionist_login`
--
ALTER TABLE `receptionist_login`
  ADD PRIMARY KEY (`Username`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
