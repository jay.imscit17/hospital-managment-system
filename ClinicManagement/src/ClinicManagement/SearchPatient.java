package ClinicManagement;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import net.proteanit.sql.DbUtils;
import javax.swing.JPanel;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JTextField;

public class SearchPatient {


					JFrame jf;
					JTable table,table2;
					private JTextField textsearchField;
					public SearchPatient()
					{
						jf=new JFrame("Update Doctor Record");
						try {
							table=new JTable();
							String  url="jdbc:mysql://localhost:3306/hospital";
							String user="root";
							String pass="";
							Class.forName("com.mysql.jdbc.Driver");
							Connection con=DriverManager.getConnection(url, user, pass);
							System.out.print("Connected ");
							Statement st=con.createStatement();
							String query="select * from add_patient";
							ResultSet rs=st.executeQuery(query);
							table.setModel(DbUtils.resultSetToTableModel(rs));
							table.setEnabled(false);
						}
						catch(Exception e)
						{
							e.printStackTrace();
						}
						jf.getContentPane().setLayout(null);
						
						JScrollPane jsp=new JScrollPane(table);
						jsp.setBounds(22, 142, 941, 293);
						jf.getContentPane().add(jsp);
						
						JPanel titlepanel = new JPanel();
						titlepanel.setBackground(Color.CYAN);
						titlepanel.setBounds(0, 11, 984, 63);
						jf.getContentPane().add(titlepanel);
						titlepanel.setLayout(null);
						
						JLabel lbltitle = new JLabel("Search Panel");
						lbltitle.setFont(new Font("Tahoma", Font.BOLD, 20));
						lbltitle.setHorizontalAlignment(SwingConstants.CENTER);
						lbltitle.setBounds(295, 11, 380, 41);
						titlepanel.add(lbltitle);
						
						//power button
						JButton btnPower = new JButton("");
						btnPower.setBounds(901, 0, 63, 63);
						titlepanel.add(btnPower);
						btnPower.setIcon(new ImageIcon("G:\\I_MSC(IT)\\Clinic Management System\\Icons\\power-button-icon-8363(1).png"));
						btnPower.setBackground(new Color(0, 191, 255));
						btnPower.addActionListener(new ActionListener() {
							
							@Override
							public void actionPerformed(ActionEvent e) {
								// TODO Auto-generated method stub
								jf.dispose();
								FirstCMPage fp=new FirstCMPage();
							}
						});
						//back button
						JButton btnBack = new JButton("");
						btnBack.setBounds(814, 0, 63, 63);
						titlepanel.add(btnBack);
						btnBack.setBackground(new Color(0, 191, 255));
						btnBack.setIcon(new ImageIcon("G:\\I_MSC(IT)\\Clinic Management System\\Icons\\back button icon(1).png"));
						btnBack.addActionListener(new ActionListener() {
							
							@Override
							public void actionPerformed(ActionEvent e) {
								// TODO Auto-generated method stub
								jf.dispose();
								new Doctor();
							}
						});
					
						textsearchField = new JTextField();
						textsearchField.setBounds(469, 85, 354, 29);
						jf.getContentPane().add(textsearchField);
						textsearchField.setColumns(10);
						
						JLabel lblupdate = new JLabel("Enter Doctor's Name to Search their Patient Records");
						lblupdate.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 16));
						lblupdate.setHorizontalAlignment(SwingConstants.CENTER);
						lblupdate.setBounds(10, 85, 438, 36);
						jf.getContentPane().add(lblupdate);
						
						JButton btnNewButton = new JButton("search");
						btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 13));
						btnNewButton.addActionListener(new ActionListener() {
							
							@Override
							public void actionPerformed(ActionEvent e) {
								// TODO Auto-generated method stub
								try
								{
									table2=new JTable();
									String url="jdbc:mysql://localhost:3306/hospital";
									String un="root";
									String pw="";
									Class.forName("com.mysql.jdbc.Driver");
									Connection con=DriverManager.getConnection(url, un, pw);
									Statement st=con.createStatement();
									String query="select * from  add_patient where Doctor_Name='"+textsearchField.getText()+"'";
									ResultSet rs=st.executeQuery(query);
									table2.setModel(DbUtils.resultSetToTableModel(rs));
									
								}
								catch(Exception ex)
								{
									ex.printStackTrace();
								}
								JScrollPane jsp=new JScrollPane(table2);
								jsp.setBounds(22, 142, 941, 293);
								jf.getContentPane().add(jsp);
							}
						});
						btnNewButton.setBounds(862, 84, 101, 29);
						
						jf.getContentPane().add(btnNewButton);
						
							
						jf.setSize(1000,500);
						jf.setVisible(true);
						jf.setLocationRelativeTo(null);
					}
//					public static void main(String[] args) {
//						// TODO Auto-generated method stub
//						new SearchPatient();
//					}
}

