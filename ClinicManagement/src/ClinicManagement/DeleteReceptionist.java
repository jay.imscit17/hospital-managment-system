package ClinicManagement;



import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import net.proteanit.sql.DbUtils;
import javax.swing.JPanel;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JTextField;

public class DeleteReceptionist {



			JFrame jf;
			JTable table,table2;
			private JTextField textdeleteField;
			public DeleteReceptionist()
			{
				jf=new JFrame("You can Delete Doctor Record");
				
				
				
				try {
					table=new JTable();
					String  url="jdbc:mysql://localhost:3306/hospital";
					String user="root";
					String pass="";
					Class.forName("com.mysql.jdbc.Driver");
					Connection con=DriverManager.getConnection(url, user, pass);
					System.out.print("Connected ");
					Statement st=con.createStatement();
					String query="select * from add_Receptionist";
					ResultSet rs=st.executeQuery(query);
					table.setModel(DbUtils.resultSetToTableModel(rs));
					table.setEnabled(false);
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
				jf.getContentPane().setLayout(null);
				
				JScrollPane jsp=new JScrollPane(table);
				jsp.setBounds(22, 142, 941, 293);
				jf.getContentPane().add(jsp);
				
				JPanel titlepanel = new JPanel();
				titlepanel.setBackground(Color.CYAN);
				titlepanel.setBounds(0, 11, 984, 63);
				jf.getContentPane().add(titlepanel);
				titlepanel.setLayout(null);
				
				JLabel lbltitle = new JLabel("Delete/View Panel");
				lbltitle.setFont(new Font("Tahoma", Font.BOLD, 20));
				lbltitle.setHorizontalAlignment(SwingConstants.CENTER);
				lbltitle.setBounds(295, 11, 380, 41);
				titlepanel.add(lbltitle);
				
				//power button
				JButton btnPower = new JButton("");
				btnPower.setBounds(901, 0, 63, 63);
				titlepanel.add(btnPower);
				btnPower.setIcon(new ImageIcon("G:\\I_MSC(IT)\\Clinic Management System\\Icons\\power-button-icon-8363(1).png"));
				btnPower.setBackground(new Color(0, 191, 255));
				btnPower.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						jf.dispose();
						FirstCMPage fp=new FirstCMPage();
					}
				});
				//back button
				JButton btnBack = new JButton("");
				btnBack.setBounds(814, 0, 63, 63);
				titlepanel.add(btnBack);
				btnBack.setBackground(new Color(0, 191, 255));
				btnBack.setIcon(new ImageIcon("G:\\I_MSC(IT)\\Clinic Management System\\Icons\\back button icon(1).png"));
				btnBack.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						jf.dispose();
						new AdminPanel();
					}
				});
			
				textdeleteField = new JTextField();
				textdeleteField.setBounds(391, 85, 354, 29);
				jf.getContentPane().add(textdeleteField);
				textdeleteField.setColumns(10);
				
				JLabel lbldeletrecord = new JLabel("Enter Receptionist Id to Delete Record");
				lbldeletrecord.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 16));
				lbldeletrecord.setHorizontalAlignment(SwingConstants.CENTER);
				lbldeletrecord.setBounds(22, 85, 324, 36);
				jf.getContentPane().add(lbldeletrecord);
				
				JButton btnNewButton = new JButton("Delete");
				btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 13));
				btnNewButton.setBounds(765, 85, 101, 29);
				btnNewButton.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						
						try {
							table2=new JTable();
							String  url="jdbc:mysql://localhost:3306/hospital";
							String user="root";
							String pass="";
							Class.forName("com.mysql.jdbc.Driver");
							Connection con=DriverManager.getConnection(url, user, pass);
							System.out.print("Connected ");
							Statement st=con.createStatement();
							String QueryDel="Delete from add_receptionist where ID='"+textdeleteField.getText()+"'";
							st.executeUpdate(QueryDel);
							String query="select * from add_receptionist";
							ResultSet rs=st.executeQuery(query);
							table2.setModel(DbUtils.resultSetToTableModel(rs));
							textdeleteField.setText("");
						}
						catch(Exception ex)
						{
							ex.printStackTrace();
						}
						JScrollPane jsp=new JScrollPane(table2);
						jsp.setBounds(22, 142, 932, 293);
						jf.getContentPane().add(jsp);
					}
				});
				jf.getContentPane().add(btnNewButton);
					
				jf.setSize(1000,500);
				jf.setVisible(true);
				jf.setLocationRelativeTo(null);
			}
//			public static void main(String[] args) {
//				// TODO Auto-generated method stub
//				new DeleteReceptionist();
//			}
		}



