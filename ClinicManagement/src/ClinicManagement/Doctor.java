package ClinicManagement;
import javax.swing.JFrame;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import java.awt.Color;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Doctor {

	JFrame jf;
	public Doctor() {
		jf=new JFrame("Doctor Panel");
		jf.getContentPane().setBackground(new Color(211, 211, 211));
		jf.setSize(700,450);
		jf.getContentPane().setLayout(null);
		
		JLabel lblWelcome = new JLabel("Panel");
		lblWelcome.setVerticalAlignment(SwingConstants.TOP);
		lblWelcome.setToolTipText("");
		lblWelcome.setOpaque(true);
		lblWelcome.setHorizontalAlignment(SwingConstants.CENTER);
		lblWelcome.setForeground(Color.WHITE);
		lblWelcome.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblWelcome.setBackground(new Color(0, 191, 255));
		lblWelcome.setBounds(0, 246, 140, 104);
		jf.getContentPane().add(lblWelcome);
		
		JLabel lblWelcome1 = new JLabel("Doctor");
		lblWelcome1.setVerticalAlignment(SwingConstants.TOP);
		lblWelcome1.setToolTipText("");
		lblWelcome1.setOpaque(true);
		lblWelcome1.setHorizontalAlignment(SwingConstants.CENTER);
		lblWelcome1.setForeground(Color.WHITE);
		lblWelcome1.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblWelcome1.setBackground(new Color(0, 191, 255));
		lblWelcome1.setBounds(0, 227, 140, 95);
		jf.getContentPane().add(lblWelcome1);
		
		//back button with icon
		JButton btnBackButton = new JButton("");
		btnBackButton.setForeground(new Color(0, 191, 255));
		btnBackButton.setBackground(new Color(0, 191, 255));
		btnBackButton.setIcon(new ImageIcon("G:\\I_MSC(IT)\\Clinic Management System\\Icons\\back button icon(1).png"));
		btnBackButton.setBounds(542, 41, 53, 48);
		btnBackButton.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				jf.dispose();
				new DoctorLogin();
			}
		});
		
		
		jf.getContentPane().add(btnBackButton);
		
		
		//power button with icon
		JButton btnPowerButton = new JButton("");
		btnPowerButton.setIcon(new ImageIcon("G:\\I_MSC(IT)\\Clinic Management System\\Icons\\power-button-icon-8363(1).png"));
		btnPowerButton.setForeground(new Color(0, 191, 255));
		btnPowerButton.setBackground(new Color(0, 191, 255));
		btnPowerButton.setBounds(605, 41, 53, 48);
		btnPowerButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				jf.dispose();
				FirstCMPage fp=new FirstCMPage();
			}
		});
		jf.getContentPane().add(btnPowerButton);

		
		//doctor panel label title
		JLabel lblDoctorTitle = new JLabel("Doctor Panel");
		lblDoctorTitle.setOpaque(true);
		lblDoctorTitle.setHorizontalAlignment(SwingConstants.CENTER);
		lblDoctorTitle.setForeground(Color.WHITE);
		lblDoctorTitle.setBackground(new Color(0, 191, 255));
		lblDoctorTitle.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblDoctorTitle.setBounds(0, 41, 684, 48);
		
		jf.getContentPane().add(lblDoctorTitle);
		
		
		//Welcome label
		JLabel lblWelcome2 = new JLabel(" Welcome");
		lblWelcome2.setVerticalAlignment(SwingConstants.BOTTOM);
		lblWelcome2.setHorizontalAlignment(SwingConstants.CENTER);
		lblWelcome2.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblWelcome2.setOpaque(true);
		lblWelcome2.setBackground(new Color(0, 191, 255));
		lblWelcome2.setForeground(new Color(255, 255, 255));
		lblWelcome2.setToolTipText("");
		lblWelcome2.setBounds(0, 143, 140, 66);
		
		jf.getContentPane().add(lblWelcome2);
		
		JLabel lblWelcome3 = new JLabel("To");
		lblWelcome3.setVerticalAlignment(SwingConstants.TOP);
		lblWelcome3.setToolTipText("");
		lblWelcome3.setOpaque(true);
		lblWelcome3.setHorizontalAlignment(SwingConstants.CENTER);
		lblWelcome3.setForeground(Color.WHITE);
		lblWelcome3.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblWelcome3.setBackground(new Color(0, 191, 255));
		lblWelcome3.setBounds(0, 208, 140, 72);
		jf.getContentPane().add(lblWelcome3);
		
		JPanel panel1 = new JPanel();
		panel1.setLayout(null);
		panel1.setBackground(Color.GRAY);
		panel1.setBounds(195, 208, 101, 94);
		jf.getContentPane().add(panel1);
		
		JButton btnUpdatePatient = new JButton("");
		btnUpdatePatient.setIcon(new ImageIcon("G:\\I_MSC(IT)\\Clinic Management System\\Icons\\update patient.png"));
		btnUpdatePatient.setBackground(Color.LIGHT_GRAY);
		btnUpdatePatient.setBounds(0, 0, 101, 78);
		btnUpdatePatient.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				jf.dispose();
				new UpdatedocPatient();
			}
		});
		panel1.add(btnUpdatePatient);
		
		JLabel lblUpdate = new JLabel("Update");
		lblUpdate.setHorizontalAlignment(SwingConstants.CENTER);
		lblUpdate.setForeground(Color.WHITE);
		lblUpdate.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblUpdate.setBounds(0, 79, 101, 15);
		panel1.add(lblUpdate);
		
		JPanel panel3 = new JPanel();
		panel3.setLayout(null);
		panel3.setBackground(Color.GRAY);
		panel3.setBounds(512, 208, 101, 94);
		jf.getContentPane().add(panel3);
		
		JButton btnSearch = new JButton("");
		btnSearch.setIcon(new ImageIcon("G:\\I_MSC(IT)\\Clinic Management System\\Icons\\search_patient.png"));
		btnSearch.setBackground(Color.LIGHT_GRAY);
		btnSearch.setBounds(0, 0, 101, 78);
	btnSearch.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				jf.dispose();
				new SearchPatient();
			}
		});
		panel3.add(btnSearch);
		
		JLabel lblSearch = new JLabel("Search");
		lblSearch.setHorizontalAlignment(SwingConstants.CENTER);
		lblSearch.setForeground(Color.WHITE);
		lblSearch.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblSearch.setBounds(0, 79, 101, 15);
		panel3.add(lblSearch);
		
		JPanel panel4 = new JPanel();
		panel4.setLayout(null);
		panel4.setBackground(Color.GRAY);
		panel4.setBounds(348, 208, 101, 94);
		jf.getContentPane().add(panel4);
		
		JButton btnView = new JButton("");
		btnView.setIcon(new ImageIcon("G:\\I_MSC(IT)\\Clinic Management System\\Icons\\view_patient.png"));
		btnView.setBackground(Color.LIGHT_GRAY);
		btnView.setBounds(0, 0, 101, 78);
		btnView.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				jf.dispose();
				
				new DocPanelView();
			}
		});
		panel4.add(btnView);
		
		JLabel lblView = new JLabel("View");
		lblView.setHorizontalAlignment(SwingConstants.CENTER);
		lblView.setForeground(Color.WHITE);
		lblView.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblView.setBounds(0, 79, 101, 15);
		panel4.add(lblView);
		jf.setVisible(true);
		jf.setLocationRelativeTo(null);
	}
/*	public static void main(String args[])
	{
		Doctor dp=new Doctor();	
		}*/
}
