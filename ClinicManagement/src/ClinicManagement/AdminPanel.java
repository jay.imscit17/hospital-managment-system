package ClinicManagement;
import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import java.awt.Color;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class AdminPanel {

	JFrame jf;
	public AdminPanel() {
		jf=new JFrame("Admin Panel");
		jf.getContentPane().setBackground(new Color(211, 211, 211));
		jf.setSize(700,450);
		jf.getContentPane().setLayout(null);
		
		JLabel lblWelcome = new JLabel("Panel");
		lblWelcome.setVerticalAlignment(SwingConstants.TOP);
		lblWelcome.setToolTipText("");
		lblWelcome.setOpaque(true);
		lblWelcome.setHorizontalAlignment(SwingConstants.CENTER);
		lblWelcome.setForeground(Color.WHITE);
		lblWelcome.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblWelcome.setBackground(new Color(0, 191, 255));
		lblWelcome.setBounds(0, 246, 140, 104);
		jf.getContentPane().add(lblWelcome);
		
		JLabel lblWelcome1 = new JLabel("Admin");
		lblWelcome1.setVerticalAlignment(SwingConstants.TOP);
		lblWelcome1.setToolTipText("");
		lblWelcome1.setOpaque(true);
		lblWelcome1.setHorizontalAlignment(SwingConstants.CENTER);
		lblWelcome1.setForeground(Color.WHITE);
		lblWelcome1.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblWelcome1.setBackground(new Color(0, 191, 255));
		lblWelcome1.setBounds(0, 227, 140, 95);
		jf.getContentPane().add(lblWelcome1);
		
		//back button with icon
		JButton btnBackButton = new JButton("");
		btnBackButton.setForeground(new Color(0, 191, 255));
		btnBackButton.setBackground(new Color(0, 191, 255));
		btnBackButton.setIcon(new ImageIcon("G:\\I_MSC(IT)\\Clinic Management System\\Icons\\back button icon(1).png"));
		btnBackButton.setBounds(542, 41, 53, 48);
		btnBackButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				jf.dispose();
				new Login();
			}
		});
		
		jf.getContentPane().add(btnBackButton);
		
		
		//power button with icon
		JButton btnPowerButton = new JButton("");
		btnPowerButton.setIcon(new ImageIcon("G:\\I_MSC(IT)\\Clinic Management System\\Icons\\power-button-icon-8363(1).png"));
		btnPowerButton.setForeground(new Color(0, 191, 255));
		btnPowerButton.setBackground(new Color(0, 191, 255));
		btnPowerButton.setBounds(605, 41, 53, 48);
		btnPowerButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				jf.dispose();
				FirstCMPage fp=new FirstCMPage();
			}
		});
		jf.getContentPane().add(btnPowerButton);
		
		
		//Admin panel label title
		JLabel lblAdminPanel = new JLabel("Admin Panel");
		lblAdminPanel.setOpaque(true);
		lblAdminPanel.setHorizontalAlignment(SwingConstants.CENTER);
		lblAdminPanel.setForeground(Color.WHITE);
		lblAdminPanel.setBackground(new Color(0, 191, 255));
		lblAdminPanel.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblAdminPanel.setBounds(0, 41, 684, 48);
		
		jf.getContentPane().add(lblAdminPanel);
		
		
		//Welcome label
		JLabel lblWelcome2 = new JLabel(" Welcome");
		lblWelcome2.setVerticalAlignment(SwingConstants.BOTTOM);
		lblWelcome2.setHorizontalAlignment(SwingConstants.CENTER);
		lblWelcome2.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblWelcome2.setOpaque(true);
		lblWelcome2.setBackground(new Color(0, 191, 255));
		lblWelcome2.setForeground(new Color(255, 255, 255));
		lblWelcome2.setToolTipText("");
		lblWelcome2.setBounds(0, 143, 140, 66);
		
		jf.getContentPane().add(lblWelcome2);
		
		JLabel lblWelcome3 = new JLabel("To");
		lblWelcome3.setVerticalAlignment(SwingConstants.TOP);
		lblWelcome3.setToolTipText("");
		lblWelcome3.setOpaque(true);
		lblWelcome3.setHorizontalAlignment(SwingConstants.CENTER);
		lblWelcome3.setForeground(Color.WHITE);
		lblWelcome3.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblWelcome3.setBackground(new Color(0, 191, 255));
		lblWelcome3.setBounds(0, 208, 140, 72);
		jf.getContentPane().add(lblWelcome3);
		
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.GRAY);
		panel.setBounds(194, 268, 124, 104);
		jf.getContentPane().add(panel);
		panel.setLayout(null);
		
		//ADD Receptionist button
		JButton btnAddReceptionist = new JButton("");
		btnAddReceptionist.setBackground(Color.LIGHT_GRAY);
		btnAddReceptionist.setIcon(new ImageIcon("G:\\I_MSC(IT)\\Clinic Management System\\Icons\\add_patient.png"));
		btnAddReceptionist.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				jf.dispose();
				new	AddReceptionist();
			}
		});
		btnAddReceptionist.setBounds(0, 0, 124, 83);
		panel.add(btnAddReceptionist);
		
		JLabel lblAddReceptionist = new JLabel("Add Receptionist");
		lblAddReceptionist.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblAddReceptionist.setHorizontalAlignment(SwingConstants.CENTER);
		lblAddReceptionist.setForeground(Color.WHITE);
		lblAddReceptionist.setBounds(0, 83, 124, 21);
		panel.add(lblAddReceptionist);
		
		JPanel panel1 = new JPanel();
		panel1.setLayout(null);
		panel1.setBackground(Color.GRAY);
		panel1.setBounds(356, 268, 149, 104);
		jf.getContentPane().add(panel1);
		
		//delete Receptionist
		JButton btnDeleteReceptionist = new JButton("");
		btnDeleteReceptionist.setIcon(new ImageIcon("G:\\I_MSC(IT)\\Clinic Management System\\Icons\\delete_patient.png"));
		btnDeleteReceptionist.setBackground(Color.LIGHT_GRAY);
		btnDeleteReceptionist.setBounds(0, 0, 149, 83);
		btnDeleteReceptionist.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				jf.dispose();
				new DeleteReceptionist();
			}
		});
		panel1.add(btnDeleteReceptionist);
		
		JLabel lblDeleteReceptionist = new JLabel("Delete/View Receptionist");
		lblDeleteReceptionist.setHorizontalAlignment(SwingConstants.CENTER);
		lblDeleteReceptionist.setForeground(Color.WHITE);
		lblDeleteReceptionist.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblDeleteReceptionist.setBounds(2, 83, 143, 21);
		panel1.add(lblDeleteReceptionist);
		
		JPanel panel2 = new JPanel();
		panel2.setLayout(null);
		panel2.setBackground(Color.GRAY);
		panel2.setBounds(194, 143, 124, 104);
		jf.getContentPane().add(panel2);
		
		//ADD Doctor Button
		JButton btnAddDoctor = new JButton("");
		btnAddDoctor.setIcon(new ImageIcon("G:\\I_MSC(IT)\\Clinic Management System\\Icons\\Add_doctor.png"));
		btnAddDoctor.setBackground(Color.LIGHT_GRAY);
		btnAddDoctor.setBounds(0, 0, 124, 83);
		btnAddDoctor.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				jf.dispose();
				new	AddDoctor();
			}
		});
		
		panel2.add(btnAddDoctor);
		
		
		
		JLabel lblAddDoctor = new JLabel("Add Doctor");
		lblAddDoctor.setHorizontalAlignment(SwingConstants.CENTER);
		lblAddDoctor.setForeground(Color.WHITE);
		lblAddDoctor.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblAddDoctor.setBounds(0, 83, 124, 21);
		panel2.add(lblAddDoctor);
		
		//Delete Doctor details 
		JPanel panel3 = new JPanel();
		panel3.setLayout(null);
		panel3.setBackground(Color.GRAY);
		panel3.setBounds(356, 143, 149, 104);
		jf.getContentPane().add(panel3);
		
		JButton btnDeleteDoctor = new JButton("");
		btnDeleteDoctor.setIcon(new ImageIcon("G:\\I_MSC(IT)\\Clinic Management System\\Icons\\delete_doc.png"));
		btnDeleteDoctor.setBackground(Color.LIGHT_GRAY);
		btnDeleteDoctor.setBounds(0, 0, 149, 83);
		btnDeleteDoctor.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				jf.dispose();
				new DeleteDoctor();
			}
		});
		panel3.add(btnDeleteDoctor);
		
		JLabel lblDeleteDoctor = new JLabel("Delete/View Doctors");
		lblDeleteDoctor.setHorizontalAlignment(SwingConstants.CENTER);
		lblDeleteDoctor.setForeground(Color.WHITE);
		lblDeleteDoctor.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblDeleteDoctor.setBounds(13, 83, 119, 21);
		panel3.add(lblDeleteDoctor);
		
		jf.setVisible(true);
		jf.setLocationRelativeTo(null);
	}
//	
//	public static void main(String args[])
//	{
//		AdminPanel ap=new AdminPanel();	
//		}
}
