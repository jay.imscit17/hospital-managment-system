package ClinicManagement;
import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import java.awt.Color;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class patient {

	JFrame jf;
	public patient() {
		jf=new JFrame("Patient Panel");
		jf.getContentPane().setBackground(new Color(211, 211, 211));
		jf.setSize(700,450);
		jf.getContentPane().setLayout(null);
		
		JLabel lblPatientWelcome = new JLabel("Patient Panel");
		lblPatientWelcome.setVerticalAlignment(SwingConstants.TOP);
		lblPatientWelcome.setToolTipText("");
		lblPatientWelcome.setOpaque(true);
		lblPatientWelcome.setHorizontalAlignment(SwingConstants.CENTER);
		lblPatientWelcome.setForeground(Color.WHITE);
		lblPatientWelcome.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblPatientWelcome.setBackground(new Color(0, 191, 255));
		lblPatientWelcome.setBounds(0, 227, 140, 95);
		jf.getContentPane().add(lblPatientWelcome);
		
		//back button with icon
		JButton btnBackButton1 = new JButton("");
		btnBackButton1.setForeground(new Color(0, 191, 255));
		btnBackButton1.setBackground(new Color(0, 191, 255));
		btnBackButton1.setIcon(new ImageIcon("G:\\I_MSC(IT)\\Clinic Management System\\Icons\\back button icon(1).png"));
		btnBackButton1.setBounds(542, 41, 53, 48);
		btnBackButton1.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				jf.dispose();
				new ReceptionistLogin();
			}
		});
		
		
		jf.getContentPane().add(btnBackButton1);
		
		
		//power button with icon
		JButton btnPowerButton1 = new JButton("");
		btnPowerButton1.setIcon(new ImageIcon("G:\\I_MSC(IT)\\Clinic Management System\\Icons\\power-button-icon-8363(1).png"));
		btnPowerButton1.setForeground(new Color(0, 191, 255));
		btnPowerButton1.setBackground(new Color(0, 191, 255));
		btnPowerButton1.setBounds(605, 41, 53, 48);
		btnPowerButton1.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				jf.dispose();
				FirstCMPage fp=new FirstCMPage();
			}
		});

		jf.getContentPane().add(btnPowerButton1);
		
		
		//patient panel label title
		JLabel lblPatient = new JLabel("Patient Panel");
		lblPatient.setOpaque(true);
		lblPatient.setHorizontalAlignment(SwingConstants.CENTER);
		lblPatient.setForeground(Color.WHITE);
		lblPatient.setBackground(new Color(0, 191, 255));
		lblPatient.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblPatient.setBounds(0, 41, 684, 48);
		
		jf.getContentPane().add(lblPatient);
		
		
		//Welcome label
		JLabel lblPatientWelcome1 = new JLabel(" Welcome");
		lblPatientWelcome1.setVerticalAlignment(SwingConstants.BOTTOM);
		lblPatientWelcome1.setHorizontalAlignment(SwingConstants.CENTER);
		lblPatientWelcome1.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblPatientWelcome1.setOpaque(true);
		lblPatientWelcome1.setBackground(new Color(0, 191, 255));
		lblPatientWelcome1.setForeground(new Color(255, 255, 255));
		lblPatientWelcome1.setToolTipText("");
		lblPatientWelcome1.setBounds(0, 143, 140, 66);
		
		jf.getContentPane().add(lblPatientWelcome1);
		
		JLabel lblPatientWelcome2 = new JLabel("To");
		lblPatientWelcome2.setVerticalAlignment(SwingConstants.TOP);
		lblPatientWelcome2.setToolTipText("");
		lblPatientWelcome2.setOpaque(true);
		lblPatientWelcome2.setHorizontalAlignment(SwingConstants.CENTER);
		lblPatientWelcome2.setForeground(Color.WHITE);
		lblPatientWelcome2.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblPatientWelcome2.setBackground(new Color(0, 191, 255));
		lblPatientWelcome2.setBounds(0, 208, 140, 72);
		jf.getContentPane().add(lblPatientWelcome2);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.GRAY);
		panel.setBounds(233, 139, 101, 94);
		jf.getContentPane().add(panel);
		panel.setLayout(null);
		
		JButton btnAddPatient = new JButton("");
		btnAddPatient.setBackground(Color.LIGHT_GRAY);
		btnAddPatient.setIcon(new ImageIcon("G:\\I_MSC(IT)\\Clinic Management System\\Icons\\add_patient.png"));
		btnAddPatient.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				jf.dispose();
				new AddPatient();
			}
		});
		btnAddPatient.setBounds(0, 0, 101, 78);
		panel.add(btnAddPatient);
		
		JLabel lblAddPatient = new JLabel("Add Patient");
		lblAddPatient.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblAddPatient.setHorizontalAlignment(SwingConstants.CENTER);
		lblAddPatient.setForeground(Color.WHITE);
		lblAddPatient.setBounds(0, 79, 101, 15);
		panel.add(lblAddPatient);
		
		JPanel panel2 = new JPanel();
		panel2.setLayout(null);
		panel2.setBackground(Color.GRAY);
		panel2.setBounds(457, 139, 101, 94);
		jf.getContentPane().add(panel2);
		
		JButton btnDeletePatient = new JButton("");
		btnDeletePatient.setIcon(new ImageIcon("G:\\I_MSC(IT)\\Clinic Management System\\Icons\\delete_patient.png"));
		btnDeletePatient.setBackground(Color.LIGHT_GRAY);
		btnDeletePatient.setBounds(0, 0, 101, 78);
		btnDeletePatient.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				jf.dispose();
				new DeletePatient();
			}
		});
		panel2.add(btnDeletePatient);
		
		JLabel lblDeletePatient = new JLabel("Delete Patient");
		lblDeletePatient.setHorizontalAlignment(SwingConstants.CENTER);
		lblDeletePatient.setForeground(Color.WHITE);
		lblDeletePatient.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblDeletePatient.setBounds(0, 79, 101, 15);
		panel2.add(lblDeletePatient);
		
		JPanel panel3 = new JPanel();
		panel3.setLayout(null);
		panel3.setBackground(Color.GRAY);
		panel3.setBounds(233, 256, 101, 94);
		jf.getContentPane().add(panel3);
		
		JButton btnSearchPatient = new JButton("");
		btnSearchPatient.setIcon(new ImageIcon("G:\\I_MSC(IT)\\Clinic Management System\\Icons\\search_patient.png"));
		btnSearchPatient.setBackground(Color.LIGHT_GRAY);
		btnSearchPatient.setBounds(0, 0, 101, 78);
		btnSearchPatient.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				jf.dispose();
				new SearchPatient();
			}
		});
		panel3.add(btnSearchPatient);
		
		JLabel lblSearchPatient = new JLabel("Search Patient");
		lblSearchPatient.setHorizontalAlignment(SwingConstants.CENTER);
		lblSearchPatient.setForeground(Color.WHITE);
		lblSearchPatient.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblSearchPatient.setBounds(0, 79, 101, 15);
		panel3.add(lblSearchPatient);
		
		JPanel panel4 = new JPanel();
		panel4.setLayout(null);
		panel4.setBackground(Color.GRAY);
		panel4.setBounds(457, 256, 101, 94);
		jf.getContentPane().add(panel4);
		
		JButton btnViewPatient = new JButton("");
		btnViewPatient.setIcon(new ImageIcon("G:\\I_MSC(IT)\\Clinic Management System\\Icons\\view_patient.png"));
		btnViewPatient.setBackground(Color.LIGHT_GRAY);
		btnViewPatient.setBounds(0, 0, 101, 78);
		btnViewPatient.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				jf.dispose();
				new ViewPatient();
			}
		});
		panel4.add(btnViewPatient);
		
		JLabel lblViewPatient = new JLabel("View Patient");
		lblViewPatient.setHorizontalAlignment(SwingConstants.CENTER);
		lblViewPatient.setForeground(Color.WHITE);
		lblViewPatient.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblViewPatient.setBounds(0, 79, 101, 15);
		panel4.add(lblViewPatient);
		jf.setVisible(true);
		jf.setLocationRelativeTo(null);
	}
//	public static void main(String args[])
//	{
//		patient pt=new patient();	
//		}
}
