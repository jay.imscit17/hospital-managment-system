package ClinicManagement;


import javax.swing.ImageIcon;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

import javax.swing.SwingConstants;
import java.awt.Font;


public class AddReceptionist {
	
	JFrame jf;
	JLabel lblname;
	JTextField txtJDate,txtId,txtName,txtAge,txtUserName,txtPhoneNumber,txtAddress,txtEmail,txtPassword,txtWardNo;
	JButton btnClear,btnAdd;
	JComboBox cbMStatus,cbGender;
	
	public AddReceptionist()
	{
		//Page Name
		jf=new JFrame("Add Receptionist Page");
		
		jf.setSize(485,398);
		jf.getContentPane().setLayout(null);
		
		JButton btnBack = new JButton("");
		btnBack.setBackground(new Color(0, 191, 255));
		btnBack.setIcon(new ImageIcon("G:\\I_MSC(IT)\\Clinic Management System\\Icons\\back button icon(1).png"));
		btnBack.setBounds(337, 11, 48, 43);
		btnBack.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				jf.dispose();
				new AdminPanel();
			}
		});
		

		
		JButton btnPower = new JButton("");
		btnPower.setIcon(new ImageIcon("G:\\I_MSC(IT)\\Clinic Management System\\Icons\\power-button-icon-8363(1).png"));
		btnPower.setBackground(new Color(0, 191, 255));
		btnPower.setBounds(395, 11, 48, 43);
		btnPower.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				jf.dispose();
				FirstCMPage fp=new FirstCMPage();
			}
		});
		jf.getContentPane().add(btnPower);
		jf.getContentPane().add(btnBack);
		
		//Panel Name
		JPanel titlepanel = new JPanel();
		titlepanel.setBackground(new Color(0, 191, 255));
		titlepanel.setBounds(0, 11, 469, 43);
		jf.getContentPane().add(titlepanel);
		titlepanel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Add Receptionist");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(136, 11, 141, 21);
		titlepanel.add(lblNewLabel);
		//Joining Date
		JLabel lblJDate = new JLabel("Joining Date");
		lblJDate.setHorizontalAlignment(SwingConstants.RIGHT);
	   	lblJDate.setBounds(9, 85, 78, 14);
	   	jf.getContentPane().add(lblJDate);
	    	
	   	txtJDate = new JTextField();
	   	txtJDate.setBounds(97, 82, 96, 20);
	   	jf.getContentPane().add(txtJDate);
	   	txtJDate.setColumns(10);
	    	
	   	//Id
	   	JLabel lblId = new JLabel("Id");
	    lblId.setBounds(27, 120, 48, 14);
	   	jf.getContentPane().add(lblId);
	    	
	   	txtId = new JTextField();
	    txtId.setColumns(10);
	   	txtId.setBounds(97, 117, 96, 20);
	   	jf.getContentPane().add(txtId);
	    	
	   	//Name
	   	JLabel lblName = new JLabel("Name");
	    lblName.setBounds(27, 151, 48, 14);
	   	jf.getContentPane().add(lblName);
	    	
	   	txtName = new JTextField();
	   	txtName.setColumns(10);
	   	txtName.setBounds(97, 148, 96, 20);
	   	jf.getContentPane().add(txtName);
	    	
	   	//Age
	    JLabel lblAge = new JLabel("Age");
	  	lblAge.setBounds(27, 183, 48, 14);
	    jf.getContentPane().add(lblAge);
	    	
	    txtAge = new JTextField();
	   	txtAge.setColumns(10);
	   	txtAge.setBounds(97, 179, 96, 20);
	   	jf.getContentPane().add(txtAge);
	    	
	   	//Gender- Drop down Menu
	   	JLabel lblGender = new JLabel("Gender");
	    lblGender.setBounds(27, 216, 48, 14);
	   	jf.getContentPane().add(lblGender);
	    	
	   	String gender[]= {"Select Item", "Male", "Female"}; 
	   	cbGender = new JComboBox(gender);
	   	cbGender.setMaximumRowCount(3);
	   	cbGender.setBounds(97, 210, 96, 20);
	   	jf.getContentPane().add(cbGender);
	   	
	   	//Clear Button
    	JButton btnClear = new JButton("Clear");
	    btnClear.setBounds(82, 286, 89, 35);
	   	jf.getContentPane().add(btnClear);
	   	
	   	//Username
	   	JLabel lblUserName = new JLabel("Username");
	   	lblUserName.setBounds(27, 244, 61, 14);
	   	jf.getContentPane().add(lblUserName);
	   	
	   	txtUserName = new JTextField();
	   	txtUserName.setBounds(97, 241, 96, 20);
	   	jf.getContentPane().add(txtUserName);
	   	txtUserName.setColumns(10);
	   	
	   	//Phone Number
	   	JLabel lblPhoneNumber = new JLabel("Phone Number");
	    lblPhoneNumber.setBounds(231, 100, 90, 14);
	   	jf.getContentPane().add(lblPhoneNumber);
	    	
	   	txtPhoneNumber = new JTextField();
	   	txtPhoneNumber.setBounds(337, 97, 96, 20);
	   	jf.getContentPane().add(txtPhoneNumber);
    	txtPhoneNumber.setColumns(10);
	    	
    	//Maritial Status drop down
	    JLabel lblMaritialStatus = new JLabel("Maritial Status");
	   	lblMaritialStatus.setBounds(231, 136, 90, 14);
	   	jf.getContentPane().add(lblMaritialStatus);
	  
	   	String status[]={"Select Item", "Married", "UnMarried"};
	 	cbMStatus = new JComboBox(status);
	   	cbMStatus.setMaximumRowCount(3);
	   	cbMStatus.setBounds(337, 133, 96, 20);
	   	jf.getContentPane().add(cbMStatus);
	   	
	   	//Address
	   	JLabel lblAddress = new JLabel("Address");
	   	lblAddress.setBounds(231, 168, 78, 14);
	   	jf.getContentPane().add(lblAddress);
	   	
	   	txtAddress = new JTextField();
	   	txtAddress.setBounds(337, 164, 96, 20);
	   	jf.getContentPane().add(txtAddress);
	   	txtAddress.setColumns(10);
	   	
	   	//Email Id
	   	JLabel lblEMail = new JLabel("Email");
	   	lblEMail.setBounds(231, 198, 48, 14);
	   	jf.getContentPane().add(lblEMail);
	   	
	   	txtEmail = new JTextField();
	   	txtEmail.setBounds(337, 195, 96, 20);
	   	jf.getContentPane().add(txtEmail);
	   	txtEmail.setColumns(10);
	   	
	   	//Password
	   	JLabel lblPassword = new JLabel("Password");
	   	lblPassword.setBounds(231, 226, 78, 14);
	   	jf.getContentPane().add(lblPassword);
	   	
	   	txtPassword = new JTextField();
	   	txtPassword.setBounds(337, 223, 96, 20);
	   	jf.getContentPane().add(txtPassword);
	   	txtPassword.setColumns(10);
	   	
	   	//Clear button sets " "
	   	btnClear.addActionListener(new ActionListener()
	    {
	   		public void actionPerformed(ActionEvent arg0)
	    	{
	   			txtJDate.setText("");
				txtId.setText("");
				txtName.setText("");
				txtAge.setText("");
				cbGender.setSelectedIndex(0); 
				txtUserName.setText("");
				txtPhoneNumber.setText("");
				cbMStatus.setSelectedIndex(0); 
				txtAddress.setText("");
				txtEmail.setText("");
				txtPassword.setText("");
    		}
	    });
	   	
	   	//Add button
	   	JButton btnAdd = new JButton("Add");
	   	btnAdd.setBounds(233, 286, 89, 35);
	   	btnAdd.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				try {
					String  url="jdbc:mysql://localhost:3306/hospital";
					String user="root";
					String pass="";
					Class.forName("com.mysql.jdbc.Driver");
					Connection con=DriverManager.getConnection(url, user, pass);
					System.out.print("Connected ");
					Statement st=con.createStatement();
					String date=txtJDate.getText();
					String id=txtId.getText();
					String name=txtName.getText();
					String age=txtAge.getText();
					String gender=(String) cbGender.getSelectedItem(); 
					String uname=txtUserName.getText();
					String phoneNo=txtPhoneNumber.getText();
					String mstatus=(String)cbMStatus.getSelectedItem(); 
					String address=txtAddress.getText();
					String email=txtEmail.getText();
					String pd=txtPassword.getText();
					String Query="insert into add_receptionist values('"+date+"','"+id+"','"+name+"','"+age+"','"+gender+"','"+uname+"','"+pd+"','"+phoneNo+"','"+mstatus+"','"+address+"','"+email+"')";
					String query2="insert into receptionist_login values('"+uname+"','"+pd+"')";
					st.executeUpdate(query2);
					int i=st.executeUpdate(Query);
					System.out.print(i+"Row inserted");
					JOptionPane.showMessageDialog(null, "Data Saved");
					txtJDate.setText("");
					txtId.setText("");
					txtName.setText("");
					txtAge.setText("");
					cbGender.setSelectedIndex(0); 
					txtUserName.setText("");
					txtPhoneNumber.setText("");
					cbMStatus.setSelectedIndex(0); 
					txtAddress.setText("");
					txtEmail.setText("");
					txtPassword.setText("");
				}
			catch(Exception ex)
			{
				ex.printStackTrace();
				JOptionPane.showMessageDialog(null,ex);
			}
			
				
			}
		});
	   	jf.getContentPane().add(btnAdd);
	   	
		jf.setVisible(true);
		jf.setLocationRelativeTo(null);
		jf.setDefaultCloseOperation(1);
	}

//	public static void main(String[] args) {
//		// TODO Auto-generated method stub
//		new	AddReceptionist();
//	}
}
