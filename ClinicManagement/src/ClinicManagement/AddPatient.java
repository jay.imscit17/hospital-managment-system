package ClinicManagement;
import javax.swing.*;
import java.awt.event.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.awt.*;
//import javax.swing.ImageIcon;

public class AddPatient
{
	JFrame jf;
	JLabel lblname;
	JTextField txtTitle,txtDate,txtId,txtName,txtAge,txtAddress,txtPhoneNumber,txtDisease,txtdocname;
	JButton btnClear,btnAdd;
	JComboBox cbGender,cbMStatus;
	
	public AddPatient()
	{
		//Page title
		jf=new JFrame("Add Patient Page");
	    	
		jf.setSize(498,385);
	    jf.getContentPane().setLayout(null);
	    
	    JButton btnPowerButton = new JButton("");
	    btnPowerButton.setIcon(new ImageIcon("G:\\I_MSC(IT)\\Clinic Management System\\Icons\\power-button-icon-8363(1).png"));
	    btnPowerButton.setBackground(new Color(0, 191, 255));
	    btnPowerButton.setBounds(399, 26, 48, 35);
		btnPowerButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				jf.dispose();
				FirstCMPage fp=new FirstCMPage();
			}
		});
	    
	    jf.getContentPane().add(btnPowerButton);
	    
	    JButton btnBackButton = new JButton("");
	    btnBackButton.setBackground(new Color(0, 191, 255));
	    btnBackButton.setIcon(new ImageIcon("G:\\I_MSC(IT)\\Clinic Management System\\Icons\\back button icon(1).png"));
	    btnBackButton.setBounds(332, 26, 48, 35);
	    btnBackButton.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				jf.dispose();
				new patient();
			}
		});
	    jf.getContentPane().add(btnBackButton);
		
	    //Date
	    JLabel lblDate = new JLabel("Date");
	   	lblDate.setBounds(27, 85, 44, 14);
	   	jf.getContentPane().add(lblDate);
	    	
	   	txtDate = new JTextField();
	   	txtDate.setBounds(82, 82, 96, 20);
	   	jf.getContentPane().add(txtDate);
	   	txtDate.setColumns(10);
	    	
	   	//Id
	   	JLabel lblId = new JLabel("Id");
	    lblId.setBounds(27, 120, 48, 14);
	   	jf.getContentPane().add(lblId);
	    	
	   	txtId = new JTextField();
	    txtId.setColumns(10);
	   	txtId.setBounds(82, 117, 96, 20);
	   	jf.getContentPane().add(txtId);
	    	
	   	//Name
	   	JLabel lblName = new JLabel("Name");
	    lblName.setBounds(23, 151, 48, 14);
	   	jf.getContentPane().add(lblName);
	    	
	   	txtName = new JTextField();
	   	txtName.setColumns(10);
	   	txtName.setBounds(82, 148, 96, 20);
	   	jf.getContentPane().add(txtName);
	    
	   	//Age
	    JLabel lblAge = new JLabel("Age");
	  	lblAge.setBounds(23, 183, 48, 14);
	    jf.getContentPane().add(lblAge);
	    	
	    txtAge = new JTextField();
	   	txtAge.setColumns(10);
	   	txtAge.setBounds(82, 179, 96, 20);
	   	jf.getContentPane().add(txtAge);
	    	
	   	//Gender
	   	JLabel lblGender = new JLabel("Gender");
	    lblGender.setBounds(27, 216, 48, 14);
	   	jf.getContentPane().add(lblGender);
	    	
	   	String gender[]= {"Select Item", "Male", "Female"}; 
	   	cbGender = new JComboBox(gender);
	   	cbGender.setMaximumRowCount(3);
	   	cbGender.setBounds(82, 210, 90, 22);
	   	jf.getContentPane().add(cbGender);
	   	
	   	//Clear Button
    	JButton btnClear = new JButton("Clear");
	    btnClear.setBounds(82, 286, 89, 35);
	   	jf.getContentPane().add(btnClear);
	    	
	   	//Address
	    JLabel lblAddress = new JLabel("Address");
	    lblAddress.setBounds(233, 85, 65, 14);
	    jf.getContentPane().add(lblAddress);
	    	
	    txtAddress = new JTextField();
	    txtAddress.setBounds(351, 82, 96, 20);
	   	jf.getContentPane().add(txtAddress);
	   	txtAddress.setColumns(10);
	    	
	   	//Phone Number
	   	JLabel lblPhoneNumber = new JLabel("Phone Number");
	    lblPhoneNumber.setBounds(233, 120, 90, 14);
	   	jf.getContentPane().add(lblPhoneNumber);
	    	
	   	txtPhoneNumber = new JTextField();
	   	txtPhoneNumber.setBounds(351, 117, 96, 20);
	   	jf.getContentPane().add(txtPhoneNumber);
    	txtPhoneNumber.setColumns(10);
	    	
    	//Maritial Status
	    JLabel lblMaritialStatus = new JLabel("Maritial Status");
	   	lblMaritialStatus.setBounds(233, 151, 90, 14);
	   	jf.getContentPane().add(lblMaritialStatus);
	    	
	  
	   	String status[]={"Select Item", "Married", "UnMarried"};
	 	cbMStatus = new JComboBox(status);
	   	cbMStatus.setMaximumRowCount(3);
	   	cbMStatus.setBounds(351, 147, 96, 20);
	   	jf.getContentPane().add(cbMStatus);
	    	
	   	//Disease Name
	    JLabel lblDiseaseName = new JLabel("Disease Name");
	   	lblDiseaseName.setBounds(233, 183, 90, 14);
	   	jf.getContentPane().add(lblDiseaseName);
	    	
	   	txtDisease = new JTextField();
	   	txtDisease.setBounds(351, 180, 96, 20);
	    jf.getContentPane().add(txtDisease);
	   	txtDisease.setColumns(10);
	    	
	   	//Doctor Name Number
	   	JLabel lbldocname = new JLabel("Doctor Name");
	   	lbldocname.setBounds(233, 216, 90, 14);
	   	jf.getContentPane().add(lbldocname);
    	
	    txtdocname = new JTextField();
	   	txtdocname.setBounds(351, 213, 96, 20);
	   	jf.getContentPane().add(txtdocname);
	   	txtdocname.setColumns(10);
	    
	   	//Clear button sets " "
	   	btnClear.addActionListener(new ActionListener()
	    {
	   		public void actionPerformed(ActionEvent arg0)
	    	{
	   			txtDate.setText("");
				txtId.setText("");
				txtName.setText("");
				txtAge.setText("");
				cbGender.setSelectedIndex(0); 
				txtAddress.setText("");
				txtPhoneNumber.setText("");
				cbMStatus.setSelectedIndex(0); 
				txtDisease.setText("");
				txtdocname.setText("");
    		}
	    });
	   	
	   	//Add Button
	    JButton btnAdd = new JButton("Add");
	   	btnAdd.setBounds(233, 286, 89, 35);
	   	btnAdd.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				try {
					String  url="jdbc:mysql://localhost:3306/hospital";
					String user="root";
					String pass="";
					Class.forName("com.mysql.jdbc.Driver");
					Connection con=DriverManager.getConnection(url, user, pass);
					System.out.print("Connected ");
					Statement st=con.createStatement();
					String date=txtDate.getText();
					String id=txtId.getText();
					String name=txtName.getText();
					String age=txtAge.getText();
					String gender=(String) cbGender.getSelectedItem(); 
					String phoneNo=txtPhoneNumber.getText();
					String mstatus=(String)cbMStatus.getSelectedItem();
					String disease=txtDisease.getText();
					String docname=txtdocname.getText();
					String address=txtAddress.getText();
					String Query="insert into add_patient values('"+date+"','"+id+"','"+name+"','"+age+"','"+gender+"','"+address+"','"+phoneNo+"','"+mstatus+"','"+disease+"','"+docname+"')";
					int i=st.executeUpdate(Query);
					System.out.print(i+"Row inserted");
					JOptionPane.showMessageDialog(null, "Data Saved");
					txtDate.setText("");
					txtId.setText("");
					txtName.setText("");
					txtAge.setText("");
					cbGender.setSelectedIndex(0); 
					txtAddress.setText("");
					txtPhoneNumber.setText("");
					cbMStatus.setSelectedIndex(0); 
					txtDisease.setText("");
					txtdocname.setText("");
				
				}
			catch(Exception ex)
			{
				ex.printStackTrace();
				JOptionPane.showMessageDialog(null,ex);
			}
			
			}
		});
	   	jf.getContentPane().add(btnAdd);
	   	
	   	JLabel lblAddPatient = new JLabel("Add Patient");
	   	lblAddPatient.setForeground(new Color(255, 255, 255));
	   	lblAddPatient.setOpaque(true);
	   	lblAddPatient.setBackground(new Color(0, 191, 255));
	   	lblAddPatient.setFont(new Font("Tahoma", Font.BOLD, 16));
	   	lblAddPatient.setHorizontalAlignment(SwingConstants.CENTER);
	   	lblAddPatient.setBounds(0, 26, 482, 35);
	   	jf.getContentPane().add(lblAddPatient);
	    
	   	jf.setVisible(true);
	   	jf.setLocationRelativeTo(null);
	   	jf.setDefaultCloseOperation(1);
	}

//	public static void main(String args[])
//	{
//		AddPatient fd=new AddPatient();
//	}
}