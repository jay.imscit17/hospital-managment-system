package ClinicManagement;


import javax.swing.JFrame;

import javax.swing.JPanel;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;

public class FirstCMPage{
	
	
	JFrame jf; 
	public FirstCMPage()
	{
		jf=new JFrame("Welcome to Clinic Management System");
		jf.setSize(800,400);
		jf.getContentPane().setLayout(null);
		
		
		
		//Title Clinic management System
		JPanel titlepanel = new JPanel();
		titlepanel.setBackground(new Color(30, 144, 255));
		titlepanel.setBounds(0, 22, 784, 50);
		titlepanel.setLayout(null);
		JLabel lbltitle = new JLabel("Clinic Management System");
		lbltitle.setBackground(new Color(0, 100, 0));
		lbltitle.setForeground(Color.WHITE);
		lbltitle.setHorizontalAlignment(SwingConstants.CENTER);
		lbltitle.setFont(new Font("Tahoma", Font.BOLD, 25));
		lbltitle.setBounds(0, 0, 784, 50);
		jf.getContentPane().add(titlepanel);
		titlepanel.add(lbltitle);
	
		//Login as
		JPanel loginaspanel = new JPanel();
		loginaspanel.setBackground(new Color(30, 144, 255));
		loginaspanel.setBounds(0, 123, 125, 50);
		loginaspanel.setLayout(null);
		JLabel lblloginas = new JLabel("Login As");
		lblloginas.setBounds(10, 10, 105, 31);
		jf.getContentPane().add(loginaspanel);
		loginaspanel.add(lblloginas);
		lblloginas.setForeground(Color.WHITE);
		lblloginas.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblloginas.setHorizontalAlignment(SwingConstants.TRAILING);
		
		
		//Welcome to mini clinic
		JPanel welcomepanel = new JPanel();
		welcomepanel.setBackground(new Color(30, 144, 255));
		welcomepanel.setBounds(0, 239, 784, 122);
		jf.add(welcomepanel);
		welcomepanel.setLayout(null);
		
		JLabel lblwelcome = new JLabel("Welcome to Mini Clinic");
		lblwelcome.setFont(new Font("Tahoma", Font.PLAIN, 34));
		lblwelcome.setHorizontalTextPosition(SwingConstants.LEFT);
		lblwelcome.setHorizontalAlignment(SwingConstants.CENTER);
		lblwelcome.setForeground(Color.WHITE);
		lblwelcome.setBounds(63, 22, 621, 77);
		welcomepanel.add(lblwelcome);
		
		//Admin Icon
		JPanel adminpanel = new JPanel();
		adminpanel.setBackground(Color.GRAY);
		adminpanel.setBounds(210, 100, 82, 93);
		jf.getContentPane().add(adminpanel);
		adminpanel.setLayout(null);
		
		JLabel lblAdmin = new JLabel("Admin");
		lblAdmin.setHorizontalTextPosition(SwingConstants.CENTER);
		lblAdmin.setBackground(new Color(255, 250, 240));
		lblAdmin.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblAdmin.setHorizontalAlignment(SwingConstants.CENTER);
		lblAdmin.setForeground(Color.WHITE);
		lblAdmin.setBounds(18, 78, 46, 14);
		adminpanel.add(lblAdmin);
		
		JButton btnAdmin = new JButton("");
		btnAdmin.setForeground(Color.LIGHT_GRAY);
		btnAdmin.setBackground(new Color(192, 192, 192));
		btnAdmin.setIcon(new ImageIcon("G:\\I_MSC(IT)\\Clinic Management System\\Icons\\Admin-ic2.png"));
		btnAdmin.setBounds(0, 0, 82, 80);
		btnAdmin.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				jf.dispose();
				Login lg=new Login();
				
			}
		});
		adminpanel.add(btnAdmin);
		
		
		
		//doctor Icon
		JPanel doctorpanel = new JPanel();
		doctorpanel.setBackground(Color.GRAY);
		doctorpanel.setBounds(395, 103, 79, 90);
		jf.add(doctorpanel);
		doctorpanel.setLayout(null);
		
		JLabel lblDoctorname = new JLabel("Doctor");
		lblDoctorname.setHorizontalAlignment(SwingConstants.CENTER);
		lblDoctorname.setForeground(Color.WHITE);
		lblDoctorname.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblDoctorname.setBounds(5, 74, 64, 14);
		doctorpanel.add(lblDoctorname);
		
		JButton btnDoctor = new JButton("");
		btnDoctor.setIcon(new ImageIcon("G:\\I_MSC(IT)\\Clinic Management System\\Icons\\People-Doctor-Male-icon.png"));
		btnDoctor.setForeground(Color.LIGHT_GRAY);
		btnDoctor.setBackground(Color.LIGHT_GRAY);
		btnDoctor.setBounds(0, 0, 79, 75);
		btnDoctor.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				jf.dispose();
				DoctorLogin lg=new DoctorLogin();
			}
		});
		doctorpanel.add(btnDoctor);
		
		// receptionist Icon
		JPanel receptionistpanel = new JPanel();
		receptionistpanel.setBackground(Color.GRAY);
		receptionistpanel.setBounds(575, 100, 90, 93);
		jf.getContentPane().add(receptionistpanel);
		receptionistpanel.setLayout(null);
		
		JLabel lblReceptionistname = new JLabel("Receptionist");
		lblReceptionistname.setHorizontalAlignment(SwingConstants.CENTER);
		lblReceptionistname.setForeground(Color.WHITE);
		lblReceptionistname.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblReceptionistname.setBounds(0, 75, 90, 14);
		receptionistpanel.add(lblReceptionistname);
		
		JButton btnReceptionist = new JButton("");
		btnReceptionist.setIcon(new ImageIcon("G:\\I_MSC(IT)\\Clinic Management System\\Icons\\receptionist-icon.png"));
		btnReceptionist.setForeground(Color.LIGHT_GRAY);
		btnReceptionist.setBackground(Color.LIGHT_GRAY);
		btnReceptionist.setBounds(0, 0, 91, 73);
		btnReceptionist.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				jf.dispose();
				ReceptionistLogin lg=new ReceptionistLogin();
			}
		});
		receptionistpanel.add(btnReceptionist);
		
		jf.setVisible(true);
		jf.setLocationRelativeTo(null);
		jf.setDefaultCloseOperation(1);
	}
	
	public static void main(String args[])
	{
		FirstCMPage fp=new FirstCMPage();
	}
}
