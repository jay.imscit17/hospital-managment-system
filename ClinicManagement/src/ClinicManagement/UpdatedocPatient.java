package ClinicManagement;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JTable;

import javax.swing.JPanel;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JTextField;



public class UpdatedocPatient {
				JFrame jf;
				JTable table;
				JLabel lblname;
				JTextField txtTitle,txtDate,txtId,txtName,txtAge,txtAddress,txtPhoneNumber,txtDisease,txtdocname;
				JButton btnClear,btnAdd;
				JComboBox cbGender,cbMStatus;
				
				private JTextField txtSearch;
				public UpdatedocPatient()
				{
					jf=new JFrame("Update Doctor Record");
					
					jf.getContentPane().setLayout(null);
					
					JPanel titlepanel = new JPanel();
					titlepanel.setBackground(Color.CYAN);
					titlepanel.setBounds(0, 11, 984, 63);
					jf.getContentPane().add(titlepanel);
					titlepanel.setLayout(null);
					
					JLabel lbltitle = new JLabel("Update Panel");
					lbltitle.setFont(new Font("Tahoma", Font.BOLD, 20));
					lbltitle.setHorizontalAlignment(SwingConstants.CENTER);
					lbltitle.setBounds(295, 11, 380, 41);
					titlepanel.add(lbltitle);
					
					//power button
					JButton btnPower = new JButton("");
					btnPower.setBounds(796, 0, 63, 63);
					titlepanel.add(btnPower);
					btnPower.setIcon(new ImageIcon("G:\\I_MSC(IT)\\Clinic Management System\\Icons\\power-button-icon-8363(1).png"));
					btnPower.setBackground(new Color(0, 191, 255));
					btnPower.addActionListener(new ActionListener() {
						
						@Override
						public void actionPerformed(ActionEvent e) {
							// TODO Auto-generated method stub
							jf.dispose();
							FirstCMPage fp=new FirstCMPage();
						}
					});
					//back button
					JButton btnBack = new JButton("");
					btnBack.setBounds(710, 0, 63, 63);
					titlepanel.add(btnBack);
					btnBack.setBackground(new Color(0, 191, 255));
					btnBack.setIcon(new ImageIcon("G:\\I_MSC(IT)\\Clinic Management System\\Icons\\back button icon(1).png"));
					btnBack.addActionListener(new ActionListener() {
						
						@Override
						public void actionPerformed(ActionEvent e) {
							// TODO Auto-generated method stub
							jf.dispose();
							new Doctor();
						}
					});
				
					txtSearch = new JTextField();
					txtSearch.setBounds(356, 86, 354, 29);
					jf.getContentPane().add(txtSearch);
					txtSearch.setColumns(10);
					
					JLabel lblupdate = new JLabel("Enter Patient name to update Record");
					lblupdate.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 16));
					lblupdate.setHorizontalAlignment(SwingConstants.CENTER);
					lblupdate.setBounds(10, 85, 331, 36);
					jf.getContentPane().add(lblupdate);
					
					JButton btnSearch = new JButton("Search");
					btnSearch.setFont(new Font("Tahoma", Font.PLAIN, 13));
					btnSearch.addActionListener(new ActionListener() {
						
						@Override
						public void actionPerformed(ActionEvent e) {
							// TODO Auto-generated method stub
							try
							{
								String url="jdbc:mysql://localhost:3306/hospital";
								String un="root";
								String pw="";
								Class.forName("com.mysql.jdbc.Driver");
								Connection con=DriverManager.getConnection(url, un, pw);
								Statement st=con.createStatement();
								String query="select * from add_patient where name='"+txtSearch.getText()+"'";
								ResultSet rs=st.executeQuery(query);
								while(rs.next())
								{
									String date=rs.getString(1);
									String id=rs.getString(2);
									String name=rs.getString(3);
									String age=rs.getString(4);
									String gender=rs.getString(5); 
									String address=rs.getString(6);
									String phoneNo=rs.getString(7);
									String mstatus=rs.getString(8);
									String disease=rs.getString(9);
									String docname=rs.getString(10);
									txtDate.setText(date);
									txtId.setText(id);
									txtName.setText(name);
									txtAge.setText(age);
									cbGender.setSelectedItem(gender);; 
									txtAddress.setText(address);
									txtPhoneNumber.setText(phoneNo);
									cbMStatus.setSelectedItem(mstatus); 
									txtDisease.setText(disease);
									txtdocname.setText(docname);
									
								}
							}
							catch(Exception ex)
							{
								ex.printStackTrace();
							}
							
						}
					});
					btnSearch.setBounds(720, 86, 101, 29);
					
					jf.getContentPane().add(btnSearch);
						
					 JLabel lblDate = new JLabel("Date");
					   	lblDate.setBounds(201, 189, 44, 14);
					   	jf.getContentPane().add(lblDate);
					    	
					   	txtDate = new JTextField();
					   	txtDate.setBounds(255, 186, 96, 20);
					   	jf.getContentPane().add(txtDate);
					   	txtDate.setColumns(10);
					    	
					   	//Id
					   	JLabel lblId = new JLabel("Id");
					    lblId.setBounds(200, 238, 48, 14);
					   	jf.getContentPane().add(lblId);
					    	
					   	txtId = new JTextField();
					    txtId.setColumns(10);
					   	txtId.setBounds(255, 235, 96, 20);
					   	jf.getContentPane().add(txtId);
					    	
					   	//Name
					   	JLabel lblName = new JLabel("Name");
					    lblName.setBounds(200, 275, 48, 14);
					   	jf.getContentPane().add(lblName);
					    	
					   	txtName = new JTextField();
					   	txtName.setColumns(10);
					   	txtName.setBounds(255, 272, 96, 20);
					   	jf.getContentPane().add(txtName);
					    
					   	//Age
					    JLabel lblAge = new JLabel("Age");
					  	lblAge.setBounds(201, 315, 48, 14);
					    jf.getContentPane().add(lblAge);
					    	
					    txtAge = new JTextField();
					   	txtAge.setColumns(10);
					   	txtAge.setBounds(255, 312, 96, 20);
					   	jf.getContentPane().add(txtAge);
					    	
					   	//Gender
					   	JLabel lblGender = new JLabel("Gender");
					    lblGender.setBounds(201, 367, 48, 14);
					   	jf.getContentPane().add(lblGender);
					    	
					   	String gender[]= {"Select Item", "Male", "Female"}; 
					   	cbGender = new JComboBox(gender);
					   	cbGender.setMaximumRowCount(3);
					   	cbGender.setBounds(255, 363, 90, 22);
					   	jf.getContentPane().add(cbGender);
					   	
					   	//Clear Button
				    	JButton btnClear = new JButton("Clear");
					    btnClear.setBounds(255, 402, 89, 35);
					   	jf.getContentPane().add(btnClear);
					    	
					   	//Address
					    JLabel lblAddress = new JLabel("Address");
					    lblAddress.setBounds(453, 186, 65, 14);
					    jf.getContentPane().add(lblAddress);
					    	
					    txtAddress = new JTextField();
					    txtAddress.setBounds(561, 175, 149, 36);
					   	jf.getContentPane().add(txtAddress);
					   	txtAddress.setColumns(10);
					    	
					   	//Phone Number
					   	JLabel lblPhoneNumber = new JLabel("Phone Number");
					    lblPhoneNumber.setBounds(453, 235, 90, 14);
					   	jf.getContentPane().add(lblPhoneNumber);
					    	
					   	txtPhoneNumber = new JTextField();
					   	txtPhoneNumber.setBounds(561, 232, 96, 20);
					   	jf.getContentPane().add(txtPhoneNumber);
				    	txtPhoneNumber.setColumns(10);
					    	
				    	//Maritial Status
					    JLabel lblMaritialStatus = new JLabel("Maritial Status");
					   	lblMaritialStatus.setBounds(453, 275, 90, 14);
					   	jf.getContentPane().add(lblMaritialStatus);
					    	
					  
					   	String status[]={"Select Item", "Married", "UnMarried"};
					 	cbMStatus = new JComboBox(status);
					   	cbMStatus.setMaximumRowCount(3);
					   	cbMStatus.setBounds(561, 272, 96, 20);
					   	jf.getContentPane().add(cbMStatus);
					    	
					   	//Disease Name
					    JLabel lblDiseaseName = new JLabel("Disease Name");
					   	lblDiseaseName.setBounds(453, 312, 90, 14);
					   	jf.getContentPane().add(lblDiseaseName);
					    	
					   	txtDisease = new JTextField();
					   	txtDisease.setBounds(561, 309, 96, 20);
					    jf.getContentPane().add(txtDisease);
					   	txtDisease.setColumns(10);
					    	
					   	//Doctor Name Number
					   	JLabel lbldocname = new JLabel("Doctor Name");
					   	lbldocname.setBounds(453, 353, 75, 14);
					   	jf.getContentPane().add(lbldocname);
				    	
					    txtdocname = new JTextField();
					   	txtdocname.setBounds(561, 350, 96, 20);
					   	jf.getContentPane().add(txtdocname);
					   	txtdocname.setColumns(10);
					    
					   	//Clear button sets " "
					   	btnClear.addActionListener(new ActionListener()
					    {
					   		public void actionPerformed(ActionEvent arg0)
					    	{
					   			txtDate.setText("");
								txtId.setText("");
								txtName.setText("");
								txtAge.setText("");
								cbGender.setSelectedIndex(0); 
								txtAddress.setText("");
								txtPhoneNumber.setText("");
								cbMStatus.setSelectedIndex(0); 
								txtDisease.setText("");
								txtdocname.setText("");
				    		}
					    });
					   	
					   	//Add Button
					    JButton btnupdate = new JButton("Update");
					   	btnupdate.setBounds(454, 402, 89, 35);
					   	btnupdate.addActionListener(new ActionListener() {
							
							@Override
							public void actionPerformed(ActionEvent e) {
								// TODO Auto-generated method stub
								try {
									String  url="jdbc:mysql://localhost:3306/hospital";
									String user="root";
									String pass="";
									Class.forName("com.mysql.jdbc.Driver");
									Connection con=DriverManager.getConnection(url, user, pass);
									System.out.print("Connected ");
									Statement st=con.createStatement();
									String date=txtDate.getText();
									String id=txtId.getText();
									String name=txtName.getText();
									String age=txtAge.getText();
									String gender=(String) cbGender.getSelectedItem(); 
									String phoneNo=txtPhoneNumber.getText();
									String mstatus=(String)cbMStatus.getSelectedItem();
									String disease=txtDisease.getText();
									String docname=txtdocname.getText();
									String address=txtAddress.getText();
									String Query="update add_patient set Date='"+date+"',ID='"+id+"',Name='"+name+"',Age='"+age+"',Gender='"+gender+"',Address='"+address+"',Phone_number='"+phoneNo+"',Maritial_status='"+mstatus+"',Disease='"+disease+"',Doctor_name='"+docname+"' where Name='"+txtSearch.getText()+"'";
									int i=st.executeUpdate(Query);
									System.out.print(i+"Row inserted");
									JOptionPane.showMessageDialog(null, "Data Updated");
									txtDate.setText("");
									txtId.setText("");
									txtName.setText("");
									txtAge.setText("");
									cbGender.setSelectedIndex(0); 
									txtAddress.setText("");
									txtPhoneNumber.setText("");
									cbMStatus.setSelectedIndex(0); 
									txtDisease.setText("");
									txtdocname.setText("");
								
								}
							catch(Exception ex)
							{
								ex.printStackTrace();
								JOptionPane.showMessageDialog(null,ex);
							}
							
							}
						});
					   	jf.getContentPane().add(btnupdate);
					   	
					   	JLabel lblAddPatient = new JLabel("Add Patient");
					   	lblAddPatient.setForeground(new Color(255, 255, 255));
					   	lblAddPatient.setOpaque(true);
					   	lblAddPatient.setBackground(new Color(0, 191, 255));
					   	lblAddPatient.setFont(new Font("Tahoma", Font.BOLD, 16));
					   	lblAddPatient.setHorizontalAlignment(SwingConstants.CENTER);
					   	lblAddPatient.setBounds(0, 26, 482, 35);
					   	jf.getContentPane().add(lblAddPatient);
					
					jf.setSize(900,500);
					jf.setVisible(true);
					jf.setLocationRelativeTo(null);
				}
//				public static void main(String[] args) {
//					// TODO Auto-generated method stub
//					new UpdatedocPatient();
//				}
}
