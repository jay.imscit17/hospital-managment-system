package ClinicManagement;


import javax.swing.JFrame;


import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import javax.swing.JTextField;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.awt.Color;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JPanel;


public class Login{

	    JFrame jf;
	    JLabel lblname,lblPassword;
	    JTextField txtName;
	    JPasswordField txtPassword;
	    JButton btnLogin,btnReset;
	    public Login()
	    {
	    	jf=new JFrame("Login Page");
	    	
	    	//Title Login AS Admin
	    	JPanel titlepanel = new JPanel();
	    	titlepanel.setBackground(Color.CYAN);
	    	titlepanel.setBounds(0, 22, 451, 44);
	    	jf.getContentPane().add(titlepanel);
	    	
	    	JLabel lbltitleLabel = new JLabel("Admin Login");
	    	lbltitleLabel.setHorizontalAlignment(SwingConstants.CENTER);
	    	lbltitleLabel.setFont(new Font("Tahoma", Font.PLAIN, 25));
	    	titlepanel.add(lbltitleLabel);
	    	
	    	
	    	lblname=new JLabel("UserName: ");
	    	lblname.setFont(new Font("Tahoma", Font.PLAIN, 13));
	    	lblname.setForeground(new Color(0, 0, 0));
	    	lblname.setBounds(73,114,100,20);
	    	
	    	
	    	lblPassword=new JLabel("Password: ");
	    	lblPassword.setFont(new Font("Tahoma", Font.PLAIN, 13));
	    	lblPassword.setForeground(new Color(0, 0, 0));
	    	lblPassword.setBounds(73,168,100,20);
	    	
	    	

	    	txtName=new JTextField(10);
	    	txtName.setBounds(196,115,179,20);
	    	
	    	txtPassword=new JPasswordField(8);
	    	txtPassword.setBounds(196,169,179,20);

	    	btnLogin=new JButton("Login");
	    	btnLogin.setForeground(new Color(255, 255, 255));//white
	    	btnLogin.setBackground(new Color(100, 149, 237));//Light blue
	    	btnLogin.setFont(new Font("Tahoma", Font.PLAIN, 17));
	    	btnLogin.setBounds(103,230,106,29);
	    	btnLogin.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					// TODO Auto-generated method stub
					// check login
					try {
						String url="jdbc:mysql://localhost:3306/hospital";
						String user="root";
						String pass="";
						Class.forName("com.mysql.jdbc.Driver");
						Connection c=DriverManager.getConnection(url, user, pass);
						System.out.print("Connected");
						Statement st=c.createStatement();
						String un=txtName.getText();
						String ps=new String(txtPassword.getPassword());
						String query="select * from admin_login where Username='"+un+"' and Password='"+ps+"'";
						ResultSet rs=st.executeQuery(query);
						System.out.print(rs);
						if(rs.next())
						{
							jf.dispose();
							AdminPanel ap=new AdminPanel();						
							JOptionPane.showMessageDialog(null,"Login Successfull");
						}
						else
						{
							JOptionPane.showMessageDialog(null,"Invalid!!");
						}
						
					}
					catch(Exception ex)
					{
						ex.printStackTrace();
					}
				}
			});
	    	
	    	
	    	btnReset=new JButton("Home");
	    	btnReset.setForeground(new Color(255, 255, 255));
	    	btnReset.setDefaultCapable(false);
	    	btnReset.setAutoscrolls(true);
	    	btnReset.setBackground(new Color(100, 149, 237));
	    	btnReset.setFont(new Font("Tahoma", Font.PLAIN, 17));
	    	btnReset.setBounds(281,230,106,29);
	    	btnReset.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					// TODO Auto-generated method stub
					jf.dispose();   // jf.dispose() on the click of a button to close JFrame.
					new FirstCMPage();
				}
			});
	    	  	
	    	
	    	jf.getContentPane().add(lblname);
	    	jf.getContentPane().add(txtName);
	    	jf.getContentPane().add(lblPassword);
	    	jf.getContentPane().add(txtPassword);
	    	jf.getContentPane().add(btnLogin);
	    	jf.getContentPane().add(btnReset);
	  
	    	
	    	jf.getContentPane().setBackground(new Color(211, 211, 211));
	    	jf.setSize(498,385);
	    	jf.getContentPane().setLayout(null);
	    	jf.setVisible(true);
	    	jf.setLocationRelativeTo(null);
	    	jf.setDefaultCloseOperation(1);
	    }
/*
public static void main(String args[])
{
	    	Login fd=new Login();
}
*/
}
